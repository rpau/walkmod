$( function() {
    $("button[data-href]").click( function() {
        location.href = $(this).attr("data-href");
    });
	
	$('.btn-contact').click(function(event){
      event.preventDefault();
      console.log('cliccck');
      $('#contact-errors').empty();
      var errorList = $('<ul></ul>');
      var errors = false;

      $('input[required]').each(function(){
        if(!$(this).val() || $(this).val() == ''){
          $(errorList).append('<li>The field '+$(this).attr('name')+' is required.</li>');
          errors = true;
        }
      });

      $('textarea[required]').each(function(){
        if(!$(this).val() || $(this).val() == ''){
          $(errorList).append('<li>The field '+$(this).attr('name')+' is required.</li>');
          errors = true;
        }
      });

      $('input[type="email"]').each(function(){
        if($(this).val() && $(this).val() != ''&& !IsEmail($(this).val()) ){
          $(errorList).append('<li>The field '+$(this).attr('name')+' is an invalid email address.</li>');
          errors = true;
        }
      });
      
      if(errors){
        $('#contact-errors').css('display','block');
        $('#contact-errors').append(errorList);
      }
      else{
        $('#contact-errors').css('display','none');
        $('#contactModal').modal('hide');  
            
        $.post('contact.php',{
          'email': $("#email").val(),
          'name': $("#name").val(),
          'comment': $("#message").val()
        }).done(function(data) {
          console.log(data);
        });

      }        
       
    });
	
	$('.code-tab').click(function(event){
      event.preventDefault();
      var active_li = $(this).parent().parent().find(".active");
      console.log(active_li);
      $(active_li).removeClass("active");

      $(this).parent().addClass("active");
      var new_target = $(this).attr('target-data');
      console.log($("#"+new_target));
      $("#"+new_target).siblings().css('display', 'none');
      
      $("#"+new_target).css('display', 'block');
      
    });
	
	prettyPrint();
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}