<?php
$groupId = $_GET["groupId"];
$artifactId = $_GET["artifactId"];
$version = $_GET["version"];

$directory = str_replace(".", "/", $groupId);
$pom = $artifactId.'-'.$version.'.pom';

$service_url = 'https://search.maven.org/remotecontent?filepath='.$directory.'/'.$artifactId.'/'.$version.'/'.$pom;

$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$curl_response = curl_exec($curl);
if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
curl_close($curl);


$sxml = simplexml_load_string($curl_response);

print json_encode($sxml);

?>