# LICENSE

Licensed under the the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version. The license is available in 
the following URL:

	http://www.gnu.org/licenses/gpl-3.0.txt

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


# THIRD PARTY

- [javaparser](http://code.google.com/p/javaparser/)
- [groovy](http://groovy.codehaus.org/)
- [spring](http://www.springsource.org/)


# BUGS AND FEATURES

If you find a bug or need a feature, please report it to <https://bitbucket.org/rpau/walkmod/issues>.
Please keep in mind that this is free software with very few volunteers working on it in
their free time.


# CONTRIBUTION

Contribution is very welcome! If you want to add a feature or fix a bug yourself,
please fork the repository at <https://bitbucket.org/rpau/walkmod>, do
your changes and send a pull request.